# Making available your presentation on GitHub and GitLab pages

## Creating GitHub pages

1. Create a new repository on [GitHub](https://github.com):
    1. Go to: Repositories > New
    2. Write a valid repository name.
    3. Select Public repository.
    4. We recommend to choose the option 'Add a README file' where you can describe the content of your repository.
    5. Click on Create repository.

<p align="center">
<img src="img/new_repo.png"
     alt="An example of the recommended options to create a new repository on GitHub: The example repository name is my-useR-repo, the repository is Public and the README file will be added."
     height="500" />
</p>

2. Add your presentation files to the repository and commit the changes.

3. In your repository: 
    1. Go to: Settings > Options > GitHub Pages.
    2. In the Source section, select Branch =  main, folder = root. 
    3. Click **Save**.

<p align="center">
<img src="img/github_pages.png"
     alt="An example of the options selected in the GitHub pages section: the branch is 'main' and the folder is 'root'."
     height="200" />
</p>

4. Go back to the GitHub Pages section and make sure the site is ready to be published, a confirmation message and the path to your site will be displayed.
    
<p align="center">
<img src="img/site_ready.png"
     alt="An example of the confirmation message: 'Your site is ready to be published at https://user.github.io/my-useR-repo/' that will be displayed once your GitHub page was correctly generated."
     height="200" />
</p>

5. Add the path to your .html presentation file at the end of the above link. If your presentation is located in the root of your repository, the final link to your presentation will be like https://user.github.io/my-useR-repo/my-presentation.html. If your presentation is located within a directory, your presentation link should looks like https://user.github.io/my-useR-repo/my-directory/my-presentation.html. 

6. You are ready to share your presentation's link! 


## Creating GitLab pages

1. Create a new repository on [GitLab](https://gitlab.com):
    1. Go to: Projects > Your projects > New project > Create blank project
    2. Write a valid repository name.
    3. Select Public repository.
    4. We recommend to choose the option 'Add a README file' where you can describe the content of your repository.
    5. Click on Create project.

<p align="center">
<img src="img/new_repo_gitlab.png"
     alt="An example of the recommended options to create a new repository on GitLab: The example repository name is my-useR-repo, the repository is Public and the README file will be added."
     height="400" />
</p>

2. Add your presentation files to the repository and commit the changes.

3. In the left panel of your repository go to: Project overview and click on Set up CI/CD under your user's name.

<p align="center">
<img src="img/setup.png"
     alt="The image shows the position of the options available under the repository author's name: Upload file, README, Add License, Add CHANGELOG, Add CONTRIBUTING, Add Kubernets cluster, Set up CI/CD (the option that we will select), and Configure Integrations."
     />
</p>

4. From the 'Apply a template' list chose HTML, or the template that best matches the kind of page you want to create. Scroll down the page, save and commit the .gitlab-ci.yml file.

5. On the left panel of your repository go to CI/CD > Pipelines to monitor the deployment of your page. It may take up to 30 minutes before the site is available after the first deployment.

6. Once the deployment has finished, go to Settings > Pages to find the link of your website. It should looks like https://user.gitlab.io/my-user-repo.

<p align="center">
<img src="img/site_ready_gitlab.png"
     alt="The image shows the result of the deployment with the link available at Settings > Pages."
     height="350" />
</p>

5. Add the path to your .html presentation file at the end of the above link. Note that the .html file of your presentation must be located in the root of your repository, the final link of your presentation should be like https://user.gitlab.io/my-user-repo/my-presentation.html. 

6. You are ready to share your presentation's link! 

For more information about GitLab pages consult <https://docs.gitlab.com/ee/user/project/pages/>

